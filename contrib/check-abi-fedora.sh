#!/bin/bash -e

dnf install -y meson git libabigail
git clone https://gitlab.freedesktop.org/hadess/check-abi
pushd check-abi
meson . _build --prefix=/usr
ninja -C _build install
popd
